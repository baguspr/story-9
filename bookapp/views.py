from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import BookInfo
from django.http import JsonResponse
import json

# Create your views here.

def index(request):
    return render(request, 'home.html')

@csrf_exempt
def CountUp(request):
    data = json.loads(request.body)
    try:
        book_details = BookInfo.objects.get(lst_id = data['id'])
        book_details.likes += 1
        book_details.save()

    except:
        book_details = BookInfo.objects.create(
            book_id = data['id'],
            author = data['volumeInfo']['authors'],
            title = data['volumeInfo']['title'],
            likes = 1
            )
    return JsonResponse({'likes': book_details.likes})

def TopList(self):
    top_list = []
    favourite = BookInfo.objects.order_by('-likes')
    for i in favourite:
        top_list.append({'title' : i.title})
    
    return JsonResponse({'top_list' : top_list})