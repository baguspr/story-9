from django.db import models

# Create your models here.

class BookInfo(models.Model):
    book_id = models.CharField(max_length=200, primary_key=True)
    author = models.CharField(max_length=200, null=True)
    title = models.CharField(max_length=200, null=True)
    likes = models.IntegerField(default=0)

    def __str__(self):
        return self.title
