var books = []

//Search Function
$(document).ready(function() {
    $("#search-bar").on("keyup", function(e) {
        var q = e.currentTarget.value.toLowerCase()
        var xhttprequest = new XMLHttpRequest();
        console.log(q);
        xhttprequest.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200 ) {
                var data = JSON.parse(this.responseText);
                lst = data.items;
                $('#search-result').html('')
                var result = '';
                for (var i = 0; i < data.items.length; i++) {
                        result += "<tr><th>" + (i + 1) + "</th>" +
                        "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                        "<td>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td>" + data.items[i].volumeInfo.authors + "</td>" +
                        "<td>" + data.items[i].volumeInfo.publishedDate + "</td>" +
                        "<td>" + "<button type='button' id='like' onclick='like("+ i +")'>LIKE</button>"  + "</td>" +
                        "<td>" + "<p id='like"+ i + "' value='0'> 0 </p>" + "</td>"
                        "</tr>"
                    }
                $('#search-result').append(result);
                }
                else {
                }
            }
        xhttprequest.open("GET", "https://www.googleapis.com/books/v1/volumes?q=" + q, true);
        xhttprequest.send();
    });
})

//Like Button Function
function like(i){
    var data = JSON.stringify(books[i]);
    var xhttprequest = new XMLHttpRequest();
    xhttprequest.onreadystatechange= function() {
        if (this.readyState == 4 && this.status == 200) {
            var like = JSON.parse(this.responseText).likes;
            $("#like" + i).html(like);
        }
    }
    xhttprequest.open("POST","CountUp");
    xhttprequest.send(data);
}

function favorites(){
    var xhttprequest = new XMLHttpRequest();
    xhttprequest.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var favs = JSON.parse(this.responseText).favorites_list;
            $('.content').html('');
            for (var i = 0; i < 5; i++){
                $('.content').append(
                    "<p>"+ favs[i]['title'] + "</p>" + "<p>"+ favs[i]['author'] + "</p>" +
                    "<p>" + favs[i]['year'] + "</p>"
                )
            }
        }
    }
    xhttprequest.open("GET", "TopList", true)
    xhttprequest.send();
}
