from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import BookInfo

class bookappUnitTest(TestCase):

    def bookapp_home_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_bookapp_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_bookapp_landing_page_contains_welcome(self):
        response = Client().get('/')
        self.assertContains(response, '<div class="header">WELCOME TO BOOK SEARCH!</div>', status_code=200)
    
    def test_booktest_can_create_new_title(self):
        #Creating a new title
        new_title = BookInfo.objects.create(title='title')

        #Retrieving all available title
        counting_all_available_title = BookInfo.objects.all().count()
        self.assertEqual(counting_all_available_title,1)
    
    def test_ftest_can_create_new_likes(self):
        #Creating a new likes
        new_likes = BookInfo.objects.create(likes=1)

        #Retrieving all available likes
        counting_all_available_likes = BookInfo.objects.all().count()
        self.assertEqual(counting_all_available_likes,1)

    def test_booktest_can_create_new_author(self):
        #Creating a new author
        new_author = BookInfo.objects.create(author='Bagus')

        #Retrieving all available author
        counting_all_available_author = BookInfo.objects.all().count()
        self.assertEqual(counting_all_available_author,1)
    
    def test_booktest_can_create_new_id(self):
        #Creating a new id
        new_id = BookInfo.objects.create(book_id=1)

        #Retrieving all available id
        counting_all_available_id = BookInfo.objects.all().count()
        self.assertEqual(counting_all_available_id,1)